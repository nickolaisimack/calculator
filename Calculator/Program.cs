﻿using System;
using System.IO;
using CalculationEngine;

namespace Calculator
{
    public class Program
    {
        static void Main(string[] args)
        {
            AddLogMessage($"[info] user input: {string.Join(' ', args)}");
            if (InputIsValid(args))
            {
                try
                {
                    decimal result = Calculation.Calculate(args);
                    Console.WriteLine($"{result}");
                    string message = $"[success] result - {result}";
                    AddLogMessage(message);
                }

                catch
                {
                    string message = "[error] critical error";
                    Console.WriteLine("Invalid operation, please, check your input");
                    AddLogMessage(message);
                }
            }
        }

        public static bool InputIsValid(string[] args)
        {
            for (int i = 0; i < args.Length; i++)
            {
                    if (!decimal.TryParse(args[i], out _) && args[i] != "+" && args[i] != "-" && args[i] != "*" && args[i] != "/" && args[i] != "(" && args[i] != ")")
                    {
                        string message = $"[error] invalid operand / operator - {args[i]}";
                        Console.WriteLine($"Please, pay attention that you can operate only with numbers and you can use these operators: +, -, *, / and you can use brackets - ( ).{Environment.NewLine}Your symbol - '{args[i]}' is incorrect");
                        AddLogMessage(message);
                        return false;
                    }
            }
            return true;
        }

        public static void AddLogMessage(string message)
        {
            File.AppendAllText($"{Constants.logFilePath}", $"{DateTime.Now.ToLongTimeString()} {DateTime.Now.ToShortDateString()} {message}{Environment.NewLine}");
        }
    }
}
