﻿using System;
using System.IO;
using CalculationEngine;
using System.Collections.Generic;

namespace CalculationEngine
{
    public class Calculation
    {
        public static decimal Calculate(string[] members)
        {
            Stack<decimal> operands = new Stack<decimal>();
            List<string> operators = new List<string>();
            Stack<string> OpeningBracketsAmount = new Stack<string>();
            Stack<string> ClosingBracketsAmount = new Stack<string>();
            decimal result = 0;
            for (int i = 0; i < members.Length; i++)
            {
                if (members[i] == "*" && members[i + 1] != "(")
                {
                    operands.Push(Convert.ToDecimal(members[i + 1]));
                    result = operands.Pop() * operands.Pop();
                    operands.Push(result);
                    i++;
                }
                else if (members[i] == "*" && members[i + 1] == "(")
                {
                    i += 2;
                    Stack<string> SymbolsForCalculationInBrackets = new Stack<string>();
                    OpeningBracketsAmount.Push("(");
                    while (OpeningBracketsAmount.Count != ClosingBracketsAmount.Count)
                    {
                        if (members[i] == "(")
                        {
                            OpeningBracketsAmount.Push(members[i]);
                            SymbolsForCalculationInBrackets.Push(members[i]);
                            i++;
                        }
                        else if (members[i] == ")")
                        {
                            ClosingBracketsAmount.Push(members[i]);
                            SymbolsForCalculationInBrackets.Push(members[i]);
                            i++;
                        }
                        else
                        {
                            SymbolsForCalculationInBrackets.Push(members[i]);
                            i++;
                        }
                    }
                    Stack<string> CalculationInBrackets = new Stack<string>(SymbolsForCalculationInBrackets.ToArray());
                    string[] SymbolsInBracketsToCalculate = CalculationInBrackets.ToArray();
                    decimal resultInBrackets = Calculate(SymbolsInBracketsToCalculate);
                    operands.Push(resultInBrackets);
                    result = operands.Pop() * operands.Pop();
                    operands.Push(result);
                    i--;
                    OpeningBracketsAmount.Clear();
                    ClosingBracketsAmount.Clear();
                }
                else if (members[i] == "/" && members[i + 1] != "(")
                {
                    decimal dividend = operands.Pop();
                    if (Convert.ToDouble(members[i + 1]) != 0)
                    {
                        operands.Push(Convert.ToDecimal(members[i + 1]));
                        result = dividend / operands.Pop();
                        operands.Push(result);
                        i += 1;
                    }
                    else
                    {
                        CatchDivisionError();
                        break;
                    }
                }
                else if (members[i] == "/" && members[i + 1] == "(")
                {
                    i += 2;
                    decimal dividend = operands.Pop();
                    Stack<string> SymbolsForCalculationInBrackets = new Stack<string>();
                    OpeningBracketsAmount.Push("(");
                    while (OpeningBracketsAmount.Count != ClosingBracketsAmount.Count)
                    {
                        if(members[i] == "(")
                        {
                            OpeningBracketsAmount.Push(members[i]);
                            SymbolsForCalculationInBrackets.Push(members[i]);
                            i++;
                        }
                        else if(members[i] == ")")
                        {
                            ClosingBracketsAmount.Push(members[i]);
                            SymbolsForCalculationInBrackets.Push(members[i]);
                            i++;
                        }
                        else
                        {
                            SymbolsForCalculationInBrackets.Push(members[i]);
                            i++;
                        }
                    }
                    Stack<string> CalculationInBrackets = new Stack<string>(SymbolsForCalculationInBrackets.ToArray());
                    string[] SymbolsInBracketsToCalculate = CalculationInBrackets.ToArray();
                    decimal resultInBrackets = Calculate(SymbolsInBracketsToCalculate);
                    operands.Push(resultInBrackets);
                    i--;
                    if (resultInBrackets != 0)
                    {
                        result = dividend / operands.Pop();
                        operands.Push(result);
                    }
                    else
                    {
                        CatchDivisionError();
                        break;
                    }
                    OpeningBracketsAmount.Clear();
                    ClosingBracketsAmount.Clear();
                }
                else if (members[i] == "(")
                {
                    i++;
                    Stack<string> SymbolsForCalculationInBrackets = new Stack<string>();
                    OpeningBracketsAmount.Push("(");
                    while (OpeningBracketsAmount.Count != ClosingBracketsAmount.Count)
                    {
                        if (members[i] == "(")
                        {
                            OpeningBracketsAmount.Push(members[i]);
                            SymbolsForCalculationInBrackets.Push(members[i]);
                            i++;
                        }
                        else if (members[i] == ")")
                        {
                            ClosingBracketsAmount.Push(members[i]);
                            SymbolsForCalculationInBrackets.Push(members[i]);
                            i++;
                        }
                        else
                        {
                            SymbolsForCalculationInBrackets.Push(members[i]);
                            i++;
                        }
                    }
                    Stack<string> CalculationInBrackets = new Stack<string>(SymbolsForCalculationInBrackets.ToArray());
                    string[] SymbolsInBracketsToCalculate = CalculationInBrackets.ToArray();
                    decimal resultInBrackets = Calculate(SymbolsInBracketsToCalculate);
                    operands.Push(resultInBrackets);
                    i--;
                    OpeningBracketsAmount.Clear();
                    ClosingBracketsAmount.Clear();
                }
                else if (members[i] == "+" || members[i] == "-")
                {
                    operators.Add(members[i]);
                }
                else if(members[i] == ")")
                {
                    if(operators != null)
                    {
                        Stack<decimal> operandsToCalculateInBrackets = new Stack<decimal>(operands.ToArray());
                        for (int z = 0; z < operators.Count; z++)
                        {
                            if(operators[z] == "-")
                            {
                                result = operandsToCalculateInBrackets.Pop() - operandsToCalculateInBrackets.Pop();
                                operandsToCalculateInBrackets.Push(result);
                            }
                            else if(operators[z] == "+")
                            {
                                result = operandsToCalculateInBrackets.Pop() + operandsToCalculateInBrackets.Pop();
                                operandsToCalculateInBrackets.Push(result);
                            }
                        }
                        operands.Clear();
                        operators.Clear();
                        operands.Push(result);
                    }
                    else
                    {
                        continue;
                    }
                }
                else
                {
                    operands.Push(Convert.ToDecimal(members[i]));
                }
            }
            Stack<decimal> operandsToCalculate = new Stack<decimal>(operands.ToArray());
            for (int i = 0; i < operators.Count; i++)
            {
                if (operators[i] == "+")
                {
                    result = operandsToCalculate.Pop() + operandsToCalculate.Pop();
                    operandsToCalculate.Push(result);
                }
                else if (operators[i] == "-")
                {
                    result = operandsToCalculate.Pop() - operandsToCalculate.Pop();
                    operandsToCalculate.Push(result);
                }
            }
            result = operandsToCalculate.Pop();
            return result;
        }
        public static void CatchDivisionError()
        {
            string message = "[error] numbers can`t be divided by zero";
            AddLogMessage(message);
            Console.WriteLine("Numbers can`t be divided by zero");
        }
        public static void AddLogMessage(string message)
        {
            File.AppendAllText($"{Path.Combine(Environment.CurrentDirectory, "Log.txt")}", $"{DateTime.Now.ToLongTimeString()} {DateTime.Now.ToShortDateString()} {message}{Environment.NewLine}");
        }
    }
}
