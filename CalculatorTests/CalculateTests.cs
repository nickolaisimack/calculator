using CalculationEngine;
using NUnit.Framework;

namespace CalculatorTests
{
    public class CalculateTests
    {
        [Test]
        [TestCase(new string[] { "1", "+", "1" }, 2)]
        [TestCase(new string[] { "1", "+", "2", "+", "3" }, 6)]
        [TestCase(new string[] { "1", "+", "3", "+", "6" }, 10)]
        [TestCase(new string[] { "1", "+", "3", "+", "-6" }, -2)]
        public void Addition(string[] input, decimal expectedResult)
        {
            Assert.AreEqual(expectedResult, Calculation.Calculate(input));
        }

        [Test]
        [TestCase(new string[] { "1", "-", "1" }, 0)]
        [TestCase(new string[] { "10", "-", "2", "-", "3" }, 5)]
        [TestCase(new string[] { "15", "-", "15", "-", "6" }, -6)]
        [TestCase(new string[] { "1", "-", "3", "-", "-6" }, 4)]
        public void Subtraction(string[] input, decimal expectedResult)
        {
            Assert.AreEqual(expectedResult, Calculation.Calculate(input));
        }

        [Test]
        [TestCase(new string[] { "1", "*", "3" }, 3)]
        [TestCase(new string[] { "10", "*", "2", "*", "3" }, 60)]
        [TestCase(new string[] { "15", "*", "2", "*", "3" }, 90)]
        [TestCase(new string[] { "1", "*", "3", "*", "-6" }, -18)]
        public void Multiplication(string[] input, decimal expectedResult)
        {
            Assert.AreEqual(expectedResult, Calculation.Calculate(input));
        }

        [Test]
        [TestCase(new string[] { "30", "/", "3" }, 10)]
        [TestCase(new string[] { "10", "/", "2", "/", "1" }, 5)]
        [TestCase(new string[] { "15", "/", "3", "/", "5" }, 1)]
        [TestCase(new string[] { "100", "/", "2", "/", "5" }, 10)]
        public void Division(string[] input, decimal expectedResult)
        {
            Assert.AreEqual(expectedResult, Calculation.Calculate(input));
        }

        [Test]
        [TestCase(new string[] { "30", "+", "(", "2", "*", "2", ")", "/", "(", "4", "/", "2", ")", "+", "5" }, 37)]
        [TestCase(new string[] { "(", "5", "*", "5", ")", "/", "(", "2,5", "*", "2", ")", "+", "5" }, 10)]
        [TestCase(new string[] { "2", "*", "(", "5", "+", "3", "/", "(", "1", "*", "1", ")", ")", "+", "5" }, 21)]
        [TestCase(new string[] { "(", "(", "3", "+", "5", ")", "*", "(", "4", "+", "1", ")", ")", "/", "4", "+", "5" }, 15)]
        [TestCase(new string[] { "(", "5", "-", "2", ")", "*", "(", "4", "/", "(", "2", "+", "2", ")", ")", "+", "5" }, 8)]
        [TestCase(new string[] { "(", "(", "3", "+", "5", ")", "/", "(", "1", "+", "1", ")", ")", "*", "3", "+", "5" }, 17)]
        [TestCase(new string[] { "(", "(", "3", "+", "5", ")", "*", "(", "1", "+", "1", ")", ")" }, 16)]
        [TestCase(new string[] { "(", "(", "3", "+", "5", ")", "/", "(", "1", "+", "1", ")", ")" }, 4)]
        [TestCase(new string[] { "(", "3", "*", "2", "*", "(", "3", "+", "1", ")", "-", "1", "-", "3", ")", "/", "5" }, 4)]
        [TestCase(new string[] { "(", "3", "*", "2", "*", "(", "3", "+", "1", ")", "+", "1", "-", "3", ")", "/", "5" }, 4.4)]
        [TestCase(new string[] { "25", "-", "(", "5", "*", "3", ")", "+", "2", "*", "(", "2", "+", "3", ")", "-", "40", "/", "2" }, 0)]
        [TestCase(new string[] { "20", "/", "(", "5", "+", "(", "2", "*", "1", ")", "-", "2", ")", "*", "2", "-", "8" }, 0)]
        [TestCase(new string[] { "20", "*", "(", "5", "+", "(", "2", "*", "1", ")", "-", "2", ")", "*", "2", "-", "8" }, 192)]
        [TestCase(new string[] { "20", "/", "(", "5", "/", "(", "2", "*", "1", ")", "-", "2", ")", "*", "2", "-", "8" }, 72)]

        public void CalculationWithBrackets(string[] input, decimal expectedResult)
        {
            Assert.AreEqual(expectedResult, Calculation.Calculate(input));
        }
    }
}